// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class DepositNavigationController: UINavigationController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var creditCardData: CreditCardData?

    override func viewDidLoad() {
        super.viewDidLoad()

        let pageController = self.viewControllers[0] as! UIPageViewController
        pageController.delegate = self
        pageController.dataSource = self
        pageController.title = "Deposit"

        pageController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                                                                           target: self,
                                                                           action: #selector(dismissPageController))

        if let creditCardData = self.creditCardData {
            let instantiateVc = self.viewControllerAtIndex(index: 0, creditCardData: creditCardData)
            pageController.setViewControllers([instantiateVc], direction: .forward, animated: false, completion: nil)
        }
    }

    @objc func dismissPageController() {
        self.dismiss(animated: true, completion: nil)
    }

    // page controller delegates
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let viewC = viewController as? PageControllerIndexDelegate, let creditCardData = self.creditCardData {
            let currentIndex = viewC.currentPageIndex - 1

            if currentIndex >= 0 {
                return self.viewControllerAtIndex(index: currentIndex, creditCardData: creditCardData)
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if let viewC = viewController as? PageControllerIndexDelegate, let creditCardData = self.creditCardData {
            let currentIndex = viewC.currentPageIndex + 1
            if currentIndex <= (self.creditCardData?.debitCards.count)! - 1 {
                return self.viewControllerAtIndex(index: currentIndex, creditCardData: creditCardData)
            }
        }
        return nil
    }

    func viewControllerAtIndex(index: Int, creditCardData: CreditCardData) -> UIViewController {

        let storyboard = UIStoryboard.init(name: "DepositScreen", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DepositController") as! DepositViewController
        viewController.currentPageIndex = index
        let depositInfo = DepositInfo(currency: creditCardData.currency,
                                      amounts: creditCardData.amountsToUse(),
                                      debitCard: creditCardData.debitCards[index],
                                      minimumAmount: creditCardData.minimumAmount,
                                      maximumAmount: creditCardData.maximumAmount)
        viewController.depositInfo = depositInfo

        return viewController
    }
}
