// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class DepositViewController: UIViewController, PageControllerIndexDelegate {

    var currentPageIndex: Int = 0 // 0 by default

    @IBOutlet weak var warningMessageLabel: UILabel!
    @IBOutlet weak var digitPicker: DigitPicker!
    @IBOutlet weak var creditCardView: CreditCardView!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var depositButton: UIButton!
    @IBOutlet weak var depositAmountLabel: UILabel!

    var amounts: [Int]!
    var debitCard: DebitCard!
    var depositInfo: DepositInfo!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let digitPicker = self.digitPicker {
            digitPicker.values = self.depositInfo.amounts
            digitPicker.delegate = self
            digitPicker.setMinimumDigit(digit: self.depositInfo.minimumAmount)
            digitPicker.setMaximumDigit(digit: self.depositInfo.maximumAmount)
        }

        self.creditCardView.cv2TextField.delegate = self

        self.depositButton.isEnabled = false

        self.depositButton.setTitleColor(UIColor.black, for: .disabled)
        self.depositButton.setTitleColor(UIColor.white, for: .normal)
        self.creditCardView.expiryDateLabel.text = depositInfo.debitCard.expiryDate
        self.creditCardView.numberLabel.text = "**** " + depositInfo.debitCard.number
        let rangeText = "Deposit amount (Min:\(depositInfo.minimumAmount) - Max: \(depositInfo.maximumAmount)"
        self.depositAmountLabel.text = rangeText

        self.addObserver(self,
                         forKeyPath: "self.digitPicker.collectionView.contentSize",
                         options: [.new, .initial], context: nil)

        let heightConstraint = (self.digitPicker.collectionView?.contentSize.height)!
        heightLayoutConstraint.constant = heightConstraint

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveWillShowNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveWillHideNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "self.digitPicker.collectionView.contentSize", context: nil)
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.digitPicker.updateLayout()
    }
    
    // actions
    @IBAction func selectAmountButtonTapped(_ sender: Any) {
        if self.isValidCV2() && self.digitPicker.isValidAmount() {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // notifications
    @objc func didReceiveWillShowNotification(notification: Notification) {
        if let view = getFirstResponder() {
            if let userInfo = notification.userInfo, let kEndRect = userInfo[UIKeyboardFrameEndUserInfoKey] {
                let rect = scrollView.convert(view.bounds, from: view)
                let y = rect.origin.y + rect.size.height
                let sy = (scrollView.bounds.size.height  - (kEndRect as! CGRect).size.height) + scrollView.contentOffset.y
                let tmp = y - sy
                if tmp > 0 {
                    var point = scrollView.contentOffset
                    point.y += tmp
                    scrollView.contentInset.bottom = tmp
                    scrollView.contentOffset = point
                }
            }
        }
    }

    @objc func didReceiveWillHideNotification(notification: Notification) {
        scrollView.contentInset.bottom = 0
    }

    // kvo observers
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey: Any]?,
                               context: UnsafeMutableRawPointer?) {
        self.updateContent()
    }

    // generic ui events
    func updateContent() {
        let heightConstraint = (self.digitPicker.collectionView?.contentSize.height)!
        heightLayoutConstraint.constant = heightConstraint + 30 // append the size of the label
    }
    
    public func updateDepositButton() {
        if self.isValidCV2() {
            self.depositButton.isEnabled = true
        } else {
            self.depositButton.isEnabled = false
        }
    }

    func showErrorMessage() {
        let message = "Sorry it has not been possible to process your deposit request. Please try again. If the problem persisits, please go to my account and select a different payment method."

        UIView.animate(withDuration: 0.15, animations: {
            self.warningMessageLabel.text = message
            self.view.layoutIfNeeded()
        })
    }

    func hideErrorMessage() {
        UIView.animate(withDuration: 0.15, animations: {
            self.warningMessageLabel.text = nil
            self.view.layoutIfNeeded()
        })
    }

    // helper
    func isValidCV2() -> Bool {
        if let cv2 = self.creditCardView.cv2TextField.text {
            if self.depositInfo.debitCard.cv2 == Int(cv2) {
                return true
            }
        }
        return false
    }
}

extension DepositViewController: DigitPickerDelegate {
    
    func digitPicker(picker: DigitPicker, didChangeDigitValue value: Int) {
        if value == 0 {
            self.depositButton.setTitle("Select amount", for: .normal)
        } else {
            self.depositButton.setTitle("Deposit " + depositInfo.currency + String(value), for: .normal)
        }
        self.updateDepositButton()
    }
}

extension DepositViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateDepositButton()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension DepositViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.hideErrorMessage()
    }
}

extension DepositViewController {
    func viewForCondition(of views: [UIView],
                          condition: (UIView) -> Bool) -> UIView? {
        for view in views {
            if condition(view) {
                return view
            } else {
                if let tmp = viewForCondition(of: view.subviews, condition: condition) {
                    return tmp
                }
            }
        }
        return nil
    }
    
    func getFirstResponder() -> UIView? {
        if let view = viewForCondition(of: self.view.subviews, condition: { (view) -> Bool in
            return view.isFirstResponder
        }) {
            return view
        }
        return nil
    }
    
    func superViewWithCondition(of view: UIView, condition: (UIView) -> Bool) -> UIView? {
        if let superview = view.superview {
            if condition(superview) {
                return view
            } else {
                if let tmp = superViewWithCondition(of: superview, condition: condition) {
                    return tmp
                }
            }
        }
        return nil
    }
}
