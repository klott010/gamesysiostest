// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    public func showAlert(message: String, title: String, dismissFormat: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: dismissFormat, style: .default, handler: nil))
        self.present(alert, animated: true)
    }

}
