// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func ferchPayments(_ sender: Any) {

        PaymentService.fetchCreditCardData { (success, creditCardData) in
            if success {
                let depositController = self.depositControlerQueue()
                depositController.creditCardData = creditCardData
                self.present(depositController, animated: true, completion: nil)
            } else {
                let message = "An error ocurred, during deposit data fetch"
                self.showAlert(message: message, title: "Warning", dismissFormat: "Ok")
            }
        }
    }

    func depositControlerQueue() -> DepositNavigationController {
        let storyboard = UIStoryboard.init(name: "DepositPageController", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DepositNavigationController") as! DepositNavigationController
    }
}
