// ---------------------------------------------------------------------------------------------------------------------

import Foundation

// ---------------------------------------------------------------------------------------------------------------------

struct CreditCardData: Codable {

    let amounts: [String]
    let currency: String
    let minimumAmount: Int
    let maximumAmount: Int
    let debitCards: [DebitCard]

    private enum CodingKeys: String, CodingKey {
        case minimumAmount = "minAmount"
        case maximumAmount = "maxAmount"
        case amounts
        case currency
        case debitCards
    }

    func amountsToUse() -> [Int] {
        return self.amounts.map { Int($0)!}
    }
}
