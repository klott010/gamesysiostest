// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

struct DebitCard: Codable {

    let id: Int
    let number: String
    let expiryDate: String
    let cv2: Int
    let representativeColor: UIColor

    private enum CodingKeys: CodingKey {
        case id
        case number
        case expiryDate
        case cv2
        case representativeColor
    }

    // decode
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        number = try container.decode(String.self, forKey: .number)
        expiryDate = try container.decode(String.self, forKey: .expiryDate)
        cv2 = try container.decode(Int.self, forKey: .cv2)

        let color = try container.decode(String.self, forKey: .representativeColor)
        representativeColor = UIColor(hexString: color)
    }

    // encode
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)

        try container.encode(number, forKey: .number)
        try container.encode(expiryDate, forKey: .expiryDate)
        try container.encode(cv2, forKey: .cv2)

        let hex = "#erddffdv"
        try container.encode(hex, forKey: .representativeColor)
    }
}
