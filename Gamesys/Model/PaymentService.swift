// ---------------------------------------------------------------------------------------------------------------------

import Foundation

// ---------------------------------------------------------------------------------------------------------------------

class PaymentService {

    static func fetchCreditCardData(fetchCompletion: @escaping ( _ success: Bool, CreditCardData?) -> Void) {
        // hardcoded URL? eventually a base URL should assigned for this payment manager like: https://api.myjson.com
        // *** and a path pattern appended like: .. bins/1azakq
        // but for testing purposes, an hardcoded URL

        let urlString = "https://api.myjson.com/bins/1azakq"

        guard let url = URL(string: urlString) else {
            fetchCompletion(false, nil)
            return
        }

        URLSession.shared.dataTask(with: url) { (data, _, error) in

            if error != nil {
                DispatchQueue.main.async {
                    fetchCompletion(false, nil)
                }
            } else {
                do {
                    guard let data = data else {
                        DispatchQueue.main.async {
                            fetchCompletion(false, nil)
                        }
                        return
                    }
                    let result = try JSONDecoder().decode(CreditCardData.self, from: data) as CreditCardData
                    DispatchQueue.main.async {
                        fetchCompletion(true, result)
                    }
                } catch {
                    DispatchQueue.main.async {
                        fetchCompletion(false, nil)
                    }
                }
            }
        }.resume()
    }
}
