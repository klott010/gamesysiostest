// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

struct DepositInfo {
    var currency: String
    var amounts: [Int]
    var debitCard: DebitCard
    let minimumAmount: Int
    let maximumAmount: Int
}
