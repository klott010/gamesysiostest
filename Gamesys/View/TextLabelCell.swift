// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class TextLabelCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
}
