// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

struct DigitPickerCellId {
    static let TextLabel = "DigitPickerCellIdTextLabel"
    static let TextField = "DigitPickerCellIdTextField"
}

protocol DigitPickerDelegate: class {
    func digitPicker(picker: DigitPicker, didChangeDigitValue value: Int)
}

class DigitPicker: UIView {

    @IBOutlet weak var collectionView: UICollectionView?

    var currentIndex: Int?
    var selectedValue: Int?
    let numberFormatter = NumberFormatter()
    
    weak var delegate: DigitPickerDelegate?

    var values: [Int]? {
        didSet {
            if let collection = self.collectionView {
                collection.reloadData()
            }
        }
    }

    public func setMinimumDigit(digit: Int) {
        self.numberFormatter.minimum = digit as NSNumber
    }

    public func setMaximumDigit(digit: Int) {
        self.numberFormatter.maximum = digit as NSNumber
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupCollectionView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        self.setupCollectionView()
        self.setupNumberFormatter()
    }

    private func setupCollectionView() {
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        self.collectionView?.reloadData()
    }
    
    private func setupNumberFormatter() {
        self.numberFormatter.numberStyle = .decimal
        self.numberFormatter.allowsFloats = false
    }
    
    public func updateLayout() {
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }

    func currentValue() -> Int {
        let indexPath = IndexPath(item: self.textFieldIndex(), section: 0)
        let cell = self.collectionView?.cellForItem(at: indexPath) as? TextFieldCell
        if let text = cell?.textField.text {
            if let number = self.numberFormatter.number(from: text) {
                return number.intValue
            }
        }
        return 0
    }

    func textFieldIndex() -> Int {
        if let collectionView = self.collectionView {
            return collectionView.numberOfItems(inSection: 0) - 1
        }
        return 0
    }

    func totalCellsCount() -> Int {
        if let values = self.values {
            return values.count + 1
        }
        return 0
    }

    public func isValidAmount() -> Bool {
        let indexPath = IndexPath(item: self.textFieldIndex(), section: 0)
        let cell = self.collectionView?.cellForItem(at: indexPath) as? TextFieldCell
        if let string = cell?.textField.text {
            let number = self.numberFormatter.number(from: string)
            if number != nil {
                return true
            }
        }
        return false
    }
    
    private func notifyValueUpdate() {
        if let delegate = self.delegate {
            let currentValue = self.currentValue()
            delegate.digitPicker(picker: self, didChangeDigitValue: currentValue)
        }
    }
}

extension DigitPicker: UITextFieldDelegate {
    
    @objc func didUpdateTextField() {
        self.notifyValueUpdate()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if let string = textField.text {
            let number = self.numberFormatter.number(from: string)
            
            if number == nil {
                textField.text = ""
            } else {
                self.notifyValueUpdate()
            }
        }
    }
    
//    func textField(_ textField: UITextField,
//                   shouldChangeCharactersIn range: NSRange,
//                   replacementString string: String) -> Bool {
//
//        if let oldString = textField.text {
//            let testString = NSString(string: oldString).replacingCharacters(in: range, with: string)
//            let number = self.numberFormatter.number(from: testString)
//
//            if number != nil {
//                self.notifyValueUpdate()
//            }
//        }
//        return true
//    }
    
}

extension DigitPicker: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let interitemSpace = self.collectionView(collectionView,
                                                 layout: collectionViewLayout,
                                                 minimumInteritemSpacingForSectionAt: indexPath.section)
        let width = collectionView.frame.width
        let itemWidth: CGFloat = (width / 3)
        let multiplier = 3 - CGFloat(indexPath.row).truncatingRemainder(dividingBy: 3.0)
        if indexPath.row == self.totalCellsCount() - 1 {
            return CGSize(width: (itemWidth * multiplier) - interitemSpace * multiplier, height: 44)
        } else {
            return CGSize(width: itemWidth - interitemSpace, height: 44)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let index = indexPath.row
        let count = collectionView.numberOfItems(inSection: indexPath.section) - 1
        if index == count {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DigitPickerCellId.TextField,
                                                          for: indexPath) as! TextFieldCell
            cell.textField.delegate = self
            cell.textField.addTarget(self, action: #selector(didUpdateTextField), for: .editingChanged)
            cell.layer.borderColor = UIColor.darkGray.cgColor
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DigitPickerCellId.TextLabel,
                                                          for: indexPath) as! TextLabelCell
            if let values = self.values {
                let valueForCell = values[indexPath.row]
                cell.textLabel.text = String(valueForCell)
                if let currentIndex = self.currentIndex {
                    if currentIndex == valueForCell {
                        cell.backgroundColor = UIColor.yellow
                        return cell
                    }
                }
                cell.backgroundColor = UIColor.purple
                return cell
            } else {
                cell.textLabel.text = nil
            }
            return cell
        }
    }
    
    // delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lastCellIndex = self.textFieldIndex()
        if indexPath.row == lastCellIndex {
            let textFieldCell = collectionView.cellForItem(at: indexPath) as! TextFieldCell
            textFieldCell.textField.becomeFirstResponder()
        } else {
            
            if let values = self.values {
                
                let value = values[indexPath.row]
                let indexPath = IndexPath(item: lastCellIndex, section: 0)
                let textFieldCell = collectionView.cellForItem(at: indexPath) as! TextFieldCell
                textFieldCell.textField.text = String(value)
                textFieldCell.resignFirstResponder()
                
                self.currentIndex = self.currentValue()
                
                if let delegate = self.delegate {
                    let currentValue = self.currentValue()
                    delegate.digitPicker(picker: self, didChangeDigitValue: currentValue)
                }
                
                collectionView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.totalCellsCount()
    }
}
