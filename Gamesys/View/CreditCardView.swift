// ---------------------------------------------------------------------------------------------------------------------

import UIKit

// ---------------------------------------------------------------------------------------------------------------------

class CreditCardView: UIView {

    @IBOutlet weak var cv2TextField: UITextField!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
}
